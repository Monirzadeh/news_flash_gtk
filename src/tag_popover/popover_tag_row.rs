use crate::util::GtkUtil;
use gdk::{EventMask, NotifyType};
use gtk::{prelude::*, subclass::prelude::*, CompositeTemplate, EventBox, Image, Inhibit, Label};
use news_flash::models::{Tag, TagID};
use parking_lot::RwLock;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/resources/ui/popover_tag.ui")]
    pub struct PopoverTagRow {
        pub id: RwLock<TagID>,

        #[template_child]
        pub tag_title: TemplateChild<Label>,
        #[template_child]
        pub tag_color: TemplateChild<Image>,
        #[template_child]
        pub remove_event: TemplateChild<EventBox>,
    }

    impl Default for PopoverTagRow {
        fn default() -> Self {
            PopoverTagRow {
                id: RwLock::new(TagID::new("")),
                tag_title: TemplateChild::default(),
                tag_color: TemplateChild::default(),
                remove_event: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PopoverTagRow {
        const NAME: &'static str = "PopoverTagRow";
        type ParentType = gtk::ListBoxRow;
        type Type = super::PopoverTagRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PopoverTagRow {}

    impl WidgetImpl for PopoverTagRow {}

    impl ContainerImpl for PopoverTagRow {}

    impl BinImpl for PopoverTagRow {}

    impl ListBoxRowImpl for PopoverTagRow {}
}

glib::wrapper! {
    pub struct PopoverTagRow(ObjectSubclass<imp::PopoverTagRow>)
        @extends gtk::Widget, gtk::ListBoxRow;
}

impl PopoverTagRow {
    pub fn new(tag: &Tag, assigned: bool) -> Self {
        let row = glib::Object::new::<Self>(&[]).unwrap();
        let imp = imp::PopoverTagRow::from_instance(&row);

        *imp.id.write() = tag.tag_id.clone();

        imp.remove_event.set_no_show_all(!assigned);
        imp.remove_event.set_events(EventMask::BUTTON_PRESS_MASK);
        imp.remove_event.set_events(EventMask::ENTER_NOTIFY_MASK);
        imp.remove_event.set_events(EventMask::LEAVE_NOTIFY_MASK);
        imp.remove_event.connect_enter_notify_event(|widget, event| {
            if event.detail() != NotifyType::Inferior {
                widget.set_opacity(1.0);
            }
            Inhibit(false)
        });
        imp.remove_event.connect_leave_notify_event(|widget, event| {
            if event.detail() != NotifyType::Inferior {
                widget.set_opacity(0.6);
            }
            Inhibit(false)
        });

        let tag_color_circle = imp.tag_color.get();
        let color = tag.color.clone();
        imp.tag_color.connect_realize(move |_widget| {
            if let Some(window) = tag_color_circle.window() {
                let scale = GtkUtil::get_scale(&tag_color_circle);
                if let Some(surface) = GtkUtil::generate_color_cirlce(&window, color.as_deref(), scale) {
                    tag_color_circle.set_from_surface(Some(&surface));
                }
            }
        });

        imp.tag_title.set_label(&tag.label);

        row
    }

    pub fn event_box(&self) -> &EventBox {
        let imp = imp::PopoverTagRow::from_instance(self);
        &imp.remove_event
    }
}
