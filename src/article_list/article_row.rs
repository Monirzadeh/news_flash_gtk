use super::models::{ArticleListArticleModel, MarkUpdate, ReadUpdate};
use crate::main_window_state::MainWindowState;
use crate::util::{DateUtil, GtkUtil, Util};
use crate::{app::Action, settings::Settings};
use chrono::NaiveDateTime;
use futures::channel::oneshot;
use futures::future::FutureExt;
use gdk::{EventType, NotifyType};
use glib::{clone, object::Cast, Sender, SignalHandlerId};
use gtk::{prelude::*, subclass::prelude::*, CompositeTemplate};
use gtk::{EventBox, Image, Inhibit, Label, Revealer, Stack, Widget};
use log::warn;
use news_flash::models::{ArticleID, FavIcon, Marked, Read, Thumbnail};
use parking_lot::RwLock;
use std::sync::Arc;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/resources/ui/article.ui")]
    pub struct ArticleRow {
        #[template_child]
        pub title_label: TemplateChild<Label>,
        #[template_child]
        pub summary_label: TemplateChild<Label>,
        #[template_child]
        pub feed_label: TemplateChild<Label>,
        #[template_child]
        pub date_label: TemplateChild<Label>,
        #[template_child]
        pub favicon: TemplateChild<Image>,
        #[template_child]
        pub thumb_image: TemplateChild<Image>,
        #[template_child]
        pub thumb_revealer: TemplateChild<Revealer>,
        #[template_child]
        pub article_eventbox: TemplateChild<EventBox>,
        #[template_child]
        pub unread_eventbox: TemplateChild<EventBox>,
        #[template_child]
        pub marked_eventbox: TemplateChild<EventBox>,
        #[template_child]
        pub unread_stack: TemplateChild<Stack>,
        #[template_child]
        pub marked_stack: TemplateChild<Stack>,
        #[template_child]
        pub marked: TemplateChild<Image>,
        #[template_child]
        pub unmarked: TemplateChild<Image>,
        #[template_child]
        pub read: TemplateChild<Image>,
        #[template_child]
        pub unread: TemplateChild<Image>,

        pub article_id: RwLock<ArticleID>,
        pub marked_handle: Arc<RwLock<Marked>>,
        pub read_handle: Arc<RwLock<Read>>,
        pub row_hovered: Arc<RwLock<bool>>,
        pub connected_signals: Vec<(SignalHandlerId, Widget)>,
    }

    impl Default for ArticleRow {
        fn default() -> Self {
            ArticleRow {
                title_label: TemplateChild::default(),
                summary_label: TemplateChild::default(),
                feed_label: TemplateChild::default(),
                date_label: TemplateChild::default(),
                favicon: TemplateChild::default(),
                thumb_image: TemplateChild::default(),
                thumb_revealer: TemplateChild::default(),
                article_eventbox: TemplateChild::default(),
                unread_eventbox: TemplateChild::default(),
                marked_eventbox: TemplateChild::default(),
                unread_stack: TemplateChild::default(),
                marked_stack: TemplateChild::default(),
                marked: TemplateChild::default(),
                unmarked: TemplateChild::default(),
                read: TemplateChild::default(),
                unread: TemplateChild::default(),
                article_id: RwLock::new(ArticleID::new("")),
                marked_handle: Arc::new(RwLock::new(Marked::Unmarked)),
                read_handle: Arc::new(RwLock::new(Read::Unread)),
                row_hovered: Arc::new(RwLock::new(false)),
                connected_signals: Vec::new(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleRow {
        const NAME: &'static str = "ArticleRow";
        type ParentType = gtk::ListBoxRow;
        type Type = super::ArticleRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleRow {}

    impl WidgetImpl for ArticleRow {}

    impl ContainerImpl for ArticleRow {}

    impl BinImpl for ArticleRow {}

    impl ListBoxRowImpl for ArticleRow {}
}

glib::wrapper! {
    pub struct ArticleRow(ObjectSubclass<imp::ArticleRow>)
        @extends gtk::Widget, gtk::ListBoxRow;
}

impl ArticleRow {
    pub fn new(
        article: &ArticleListArticleModel,
        state: &Arc<RwLock<MainWindowState>>,
        settings: &Arc<RwLock<Settings>>,
        sender: Sender<Action>,
    ) -> Self {
        let row = glib::Object::new::<Self>(&[]).unwrap();
        let imp = imp::ArticleRow::from_instance(&row);

        let scale = GtkUtil::get_scale(&row);

        let surface = GtkUtil::create_surface_from_icon_name("marked", 16, scale);
        imp.marked.set_from_surface(Some(&surface));

        let surface = GtkUtil::create_surface_from_icon_name("unmarked", 16, scale);
        imp.unmarked.set_from_surface(Some(&surface));

        let surface = GtkUtil::create_surface_from_icon_name("read", 16, scale);
        imp.read.set_from_surface(Some(&surface));

        let surface = GtkUtil::create_surface_from_icon_name("unread", 16, scale);
        imp.unread.set_from_surface(Some(&surface));

        imp.title_label.set_text(&article.title);
        imp.title_label.set_tooltip_text(Some(&article.title));
        imp.summary_label.set_text(&article.summary);
        imp.feed_label.set_text(&article.feed_title);
        imp.date_label.set_text(&DateUtil::format(&article.date));

        // load favicon
        let favicon = imp.favicon.get();
        let (oneshot_sender, receiver) = oneshot::channel::<Option<FavIcon>>();
        Util::send(&sender, Action::LoadFavIcon((article.feed_id.clone(), oneshot_sender)));
        let glib_future = receiver.map(move |res| match res {
            Ok(Some(icon)) => {
                if let Some(data) = &icon.data {
                    if let Ok(surface) = GtkUtil::create_surface_from_bytes(data, 16, 16, scale) {
                        favicon.set_from_surface(Some(&surface));
                    }
                }
            }
            Ok(None) => {
                warn!("Favicon does not contain image data.");
            }
            Err(_) => warn!("Receiving favicon failed."),
        });
        Util::glib_spawn_future(glib_future);

        // load thumbnail
        if settings.read().get_article_list_show_thumbs() {
            let title_label = imp.title_label.get();
            let summary_label = imp.summary_label.get();
            let thumb_image = imp.thumb_image.get();
            let thumb_revealer = imp.thumb_revealer.get();
            let (oneshot_sender, receiver) = oneshot::channel::<Option<Thumbnail>>();
            Util::send(&sender, Action::LoadThumbnail((article.id.clone(), oneshot_sender)));
            let glib_future = receiver.map(move |res| match res {
                Ok(Some(thumb)) => {
                    let (width, height) = Self::calculate_thumbnail_size(thumb.width, thumb.height, 64);
                    if let Some(data) = &thumb.data {
                        if let Ok(surface) = GtkUtil::create_surface_from_bytes(data, width, height, scale) {
                            thumb_image.set_from_surface(Some(&surface));
                            summary_label.set_visible(false);
                            title_label.set_lines(3);
                            thumb_revealer.set_reveal_child(true);
                        }
                    }
                }
                Ok(None) => {
                    log::debug!("Thumbnail does not contain image data.");
                }
                Err(_) => warn!("Receiving favicon failed."),
            });
            Util::glib_spawn_future(glib_future);
        }

        *imp.article_id.write() = article.id.clone();
        *imp.read_handle.write() = article.read;
        *imp.marked_handle.write() = article.marked;

        let mut connected_signals = Vec::new();

        connected_signals.append(&mut Self::setup_row_eventbox(
            &imp.article_eventbox,
            &imp.read_handle,
            &imp.marked_handle,
            &imp.unread_stack,
            &imp.marked_stack,
            &imp.title_label,
            &imp.row_hovered,
        ));
        connected_signals.append(&mut Self::setup_unread_eventbox(
            &sender,
            state,
            &imp.unread_eventbox,
            &imp.read_handle,
            &imp.unread_stack,
            &article.id,
        ));
        connected_signals.append(&mut Self::setup_marked_eventbox(
            &sender,
            state,
            &imp.marked_eventbox,
            &imp.marked_handle,
            &imp.marked_stack,
            &article.id,
        ));

        row
    }

    pub fn update_marked(&self, marked: Marked) {
        let imp = imp::ArticleRow::from_instance(self);
        Self::update_marked_stack(&imp.marked_stack.get(), marked);
        *imp.marked_handle.write() = marked;
    }

    pub fn update_unread(&self, unread: Read) {
        let imp = imp::ArticleRow::from_instance(self);
        Self::update_title_label(&imp.title_label.get(), unread);
        Self::update_unread_stack(&imp.unread_stack.get(), unread, *imp.row_hovered.read());
        *imp.read_handle.write() = unread;
    }

    pub fn update_date_string(&self, date: NaiveDateTime) {
        let imp = imp::ArticleRow::from_instance(self);
        imp.date_label.set_text(&DateUtil::format(&date));
    }

    fn calculate_thumbnail_size(thumb_width: Option<i32>, thumb_height: Option<i32>, target_size: i32) -> (i32, i32) {
        if let Some(width) = thumb_width {
            if let Some(height) = thumb_height {
                if width != height {
                    let aspect_ratio = width as f64 / height as f64;
                    if width > height {
                        (target_size, (target_size as f64 / aspect_ratio) as i32)
                    } else {
                        ((target_size as f64 * aspect_ratio) as i32, target_size)
                    }
                } else {
                    (target_size, target_size)
                }
            } else {
                (target_size, target_size)
            }
        } else {
            (target_size, target_size)
        }
    }

    fn setup_unread_eventbox(
        sender: &Sender<Action>,
        state: &Arc<RwLock<MainWindowState>>,
        eventbox: &EventBox,
        read: &Arc<RwLock<Read>>,
        unread_stack: &Stack,
        article_id: &ArticleID,
    ) -> Vec<(SignalHandlerId, Widget)> {
        let mut vec = Vec::new();
        vec.push((
            eventbox.connect_enter_notify_event(clone!(
                @weak unread_stack,
                @weak state as window_state,
                @weak read => @default-panic, move |_widget, _event|
            {
                if !window_state.read().get_offline() {
                    match *read.read() {
                        Read::Unread => unread_stack.set_visible_child_name("read"),
                        Read::Read => unread_stack.set_visible_child_name("unread"),
                    }
                }
                Inhibit(false)
            })),
            eventbox.clone().upcast::<Widget>(),
        ));

        vec.push((
            eventbox.connect_leave_notify_event(clone!(
                @weak state as window_state,
                @weak unread_stack,
                @weak read => @default-panic, move |_widget, _event|
            {
                if !window_state.read().get_offline() {
                    match *read.read() {
                        Read::Unread => unread_stack.set_visible_child_name("unread"),
                        Read::Read => unread_stack.set_visible_child_name("read"),
                    }
                }
                Inhibit(false)
            })),
            eventbox.clone().upcast::<Widget>(),
        ));

        vec.push((
            eventbox.connect_button_press_event(clone!(
                @weak state as window_state,
                @weak read,
                @strong article_id,
                @strong sender => @default-panic, move |_widget, event|
            {
                if event.button() != 1 {
                    return Inhibit(false);
                }
                match event.event_type() {
                    EventType::ButtonRelease | EventType::DoubleButtonPress | EventType::TripleButtonPress => {
                        return Inhibit(false);
                    }
                    _ => {}
                }
                if window_state.read().get_offline() {
                    return Inhibit(false);
                }

                let new_state = read.read().invert();
                *read.write() = new_state;
                let update = ReadUpdate {
                    article_id: article_id.clone(),
                    read: new_state,
                };
                Util::send(&sender, Action::MarkArticleRead(update));
                Inhibit(true)
            })),
            eventbox.clone().upcast::<Widget>(),
        ));

        vec
    }

    fn setup_marked_eventbox(
        sender: &Sender<Action>,
        state: &Arc<RwLock<MainWindowState>>,
        eventbox: &EventBox,
        marked: &Arc<RwLock<Marked>>,
        marked_stack: &Stack,
        article_id: &ArticleID,
    ) -> Vec<(SignalHandlerId, Widget)> {
        let mut vec = Vec::new();

        vec.push((
            eventbox.connect_enter_notify_event(clone!(
                @weak marked_stack,
                @weak state as window_state,
                @weak marked => @default-panic, move |_widget, _event|
            {
                if !window_state.read().get_offline() {
                    match *marked.read() {
                        Marked::Marked => marked_stack.set_visible_child_name("unmarked"),
                        Marked::Unmarked => marked_stack.set_visible_child_name("marked"),
                    }
                }
                Inhibit(false)
            })),
            eventbox.clone().upcast::<Widget>(),
        ));

        vec.push((
            eventbox.connect_leave_notify_event(clone!(
                @weak marked_stack,
                @weak state as window_state,
                @weak marked => @default-panic, move |_widget, _event|
            {
                if !window_state.read().get_offline() {
                    match *marked.read() {
                        Marked::Marked => marked_stack.set_visible_child_name("marked"),
                        Marked::Unmarked => marked_stack.set_visible_child_name("unmarked"),
                    }
                }
                Inhibit(false)
            })),
            eventbox.clone().upcast::<Widget>(),
        ));

        vec.push((
            eventbox.connect_button_press_event(clone!(
                @strong sender,
                @strong article_id,
                @weak state as window_state,
                @weak marked => @default-panic, move |_widget, event|
            {
                if event.button() != 1 {
                    return Inhibit(false);
                }
                match event.event_type() {
                    EventType::ButtonRelease | EventType::DoubleButtonPress | EventType::TripleButtonPress => {
                        return Inhibit(false);
                    }
                    _ => {}
                }
                if window_state.read().get_offline() {
                    return Inhibit(false);
                }
                let new_marked = marked.read().invert();
                *marked.write() = new_marked;

                let update = MarkUpdate {
                    article_id: article_id.clone(),
                    marked: new_marked,
                };
                Util::send(&sender, Action::MarkArticle(update));
                Inhibit(true)
            })),
            eventbox.clone().upcast::<Widget>(),
        ));

        vec
    }

    fn setup_row_eventbox(
        eventbox: &EventBox,
        read: &Arc<RwLock<Read>>,
        marked: &Arc<RwLock<Marked>>,
        unread_stack: &Stack,
        marked_stack: &Stack,
        title_label: &Label,
        row_hovered: &Arc<RwLock<bool>>,
    ) -> Vec<(SignalHandlerId, Widget)> {
        Self::update_title_label(&title_label, *read.read());
        Self::update_unread_stack(&unread_stack, *read.read(), *row_hovered.read());
        Self::update_marked_stack(&marked_stack, *marked.read());

        let mut vec = Vec::new();

        vec.push((
            eventbox.connect_enter_notify_event(clone!(
                @weak row_hovered,
                @weak unread_stack,
                @weak marked_stack,
                @weak marked,
                @weak read => @default-panic, move |_widget, event|
            {
                if event.detail() == NotifyType::Inferior {
                    return Inhibit(false);
                }
                *row_hovered.write() = true;
                match *read.read() {
                    Read::Read => unread_stack.set_visible_child_name("read"),
                    Read::Unread => unread_stack.set_visible_child_name("unread"),
                }
                match *marked.read() {
                    Marked::Marked => marked_stack.set_visible_child_name("marked"),
                    Marked::Unmarked => marked_stack.set_visible_child_name("unmarked"),
                }
                Inhibit(true)
            })),
            eventbox.clone().upcast::<Widget>(),
        ));

        vec.push((
            eventbox.connect_leave_notify_event(clone!(
                @weak row_hovered,
                @weak marked_stack,
                @weak unread_stack,
                @weak marked,
                @weak read => @default-panic, move |_widget, event|
            {
                if event.detail() == NotifyType::Inferior {
                    return Inhibit(false);
                }
                *row_hovered.write() = false;
                match *read.read() {
                    Read::Read => unread_stack.set_visible_child_name("empty"),
                    Read::Unread => unread_stack.set_visible_child_name("unread"),
                }
                match *marked.read() {
                    Marked::Marked => marked_stack.set_visible_child_name("marked"),
                    Marked::Unmarked => marked_stack.set_visible_child_name("empty"),
                }
                Inhibit(true)
            })),
            eventbox.clone().upcast::<Widget>(),
        ));

        vec
    }

    fn update_title_label(title_label: &Label, read: Read) {
        let context = title_label.style_context();
        match read {
            Read::Read => context.remove_class("bold"),
            Read::Unread => context.add_class("bold"),
        }
    }

    fn update_unread_stack(unread_stack: &Stack, read: Read, row_hovered: bool) {
        match read {
            Read::Read => {
                if row_hovered {
                    unread_stack.set_visible_child_name("read")
                } else {
                    unread_stack.set_visible_child_name("empty")
                }
            }
            Read::Unread => unread_stack.set_visible_child_name("unread"),
        }
    }

    fn update_marked_stack(marked_stack: &Stack, marked: Marked) {
        match marked {
            Marked::Unmarked => marked_stack.set_visible_child_name("empty"),
            Marked::Marked => marked_stack.set_visible_child_name("marked"),
        }
    }

    pub fn article_id(&self) -> ArticleID {
        let imp = imp::ArticleRow::from_instance(self);
        imp.article_id.read().clone()
    }

    pub fn read(&self) -> Read {
        let imp = imp::ArticleRow::from_instance(self);
        imp.read_handle.read().clone()
    }

    pub fn marked(&self) -> Marked {
        let imp = imp::ArticleRow::from_instance(self);
        imp.marked_handle.read().clone()
    }
}
