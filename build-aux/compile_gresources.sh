#!/bin/sh
# Source: https://gitlab.gnome.org/World/podcasts/

glib-compile-resources --sourcedir=../data/ --target=../data/resources/gresource_bundles/symbolic_icons.gresource ../data/symbolic_icons.gresource.xml
glib-compile-resources --sourcedir=../data/ --target=../data/resources/gresource_bundles/ui_templates.gresource ../data/ui_templates.gresource.xml
